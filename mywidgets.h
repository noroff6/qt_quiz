#ifndef MYWIDGETS_H
#define MYWIDGETS_H

#include <QMainWindow>
#include "dialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class mywidgets; }
QT_END_NAMESPACE

class mywidgets : public QMainWindow
{
    Q_OBJECT

public:
    mywidgets(QWidget *parent = nullptr);
    ~mywidgets();
    void SetMyText(QString text);

private slots:
    void on_pushButton_clicked();
    void on_lineEdit_returnPressed();

private:
    Ui::mywidgets *ui;
    Dialog *dialog;
};
#endif // MYWIDGETS_H
