#include "mywidgets.h"
#include "ui_mywidgets.h"
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QHBoxLayout>


mywidgets::mywidgets(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::mywidgets)
{
    ui->setupUi(this);
    //setStyleSheet( "QWidget{ background-color : rgba( 160, 160, 160, 255); border-radius : 7px;  }" );
}

mywidgets::~mywidgets()
{
    delete ui;
}




void mywidgets::on_pushButton_clicked()
{
    QString name = ui->lineEdit->text();;
    hide();
    dialog = new Dialog(this, name);
    dialog->show();
}


void mywidgets::on_lineEdit_returnPressed()
{
    on_pushButton_clicked();
    /*QString name = ui->lineEdit->text();
    hide();
    dialog = new Dialog(this, name);
    dialog->show();*/

}



