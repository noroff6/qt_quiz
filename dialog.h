#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr, QString name = "");
    ~Dialog();

private slots:
    void on_label_2_objectNameChanged(const QString &objectName);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
